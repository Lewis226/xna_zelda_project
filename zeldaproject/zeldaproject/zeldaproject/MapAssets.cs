﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace ZeldaProject
{
    public class MapAssets
    {
        ContentManager content;
        public List<Texture2D> textureList = new List<Texture2D>();

        public int tileSheetWidth { get; set; }
        public int tileSheetHeight { get; set; }

        Texture2D tileSheet;

        public MapAssets(ContentManager content)
        {
            this.content = content;
            tileSheetWidth = 24;
            tileSheetHeight = 25;

            Console.WriteLine("Tile textures loading... ");
            loadTiles();
            Console.WriteLine("Tiles Textures loaded!");
        }

        public void loadTiles()
        {
            Texture2D temp;


            for (int i = 0; i < 549; i++)
            {
                Console.WriteLine("Loading texture " + (i + 1) + " of 549");
                temp = content.Load<Texture2D>("Tiles/" + i);
                textureList.Add(temp);
            }

            Console.WriteLine("Loading tilesheets...");
            tileSheet = content.Load<Texture2D>("Tiles/tileSheet");
            Console.WriteLine("Tilesheets loaded!");

            temp = null; 
        }

        public Texture2D getTileSheet()
        {
            return tileSheet;
        }

        public List<Texture2D> getTexturesList()
        {
            return textureList;
        }


    }
}
