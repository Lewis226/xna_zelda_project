﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZeldaProject
{
    public class Tile
    {
        protected Vector2 position      { get; set; }
        private Rectangle bounds        { get; set; }
        protected Texture2D texture     { get; set; }
        protected bool isCollide        { get; set; }

        public Tile(Texture2D texture, Vector2 position, bool isCollide)
        {
            this.position = position;
            this.texture = texture;
            this.bounds = new Rectangle((int)position.X, (int)position.Y, (int)texture.Width, (int)texture.Height);
            this.isCollide = isCollide;
        }

        virtual public void draw(SpriteBatch batch)
        {
            batch.Draw(texture, position, Color.White);
        }
    }
}
