﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ZeldaProject
{
    public class Entity
    {
        protected Texture2D texture { get; set; }
        protected Vector2 position  { get; set; }
        protected Vector2 velocity  { get; set; }
        protected Vector2 origin    { get; set; }
        protected Vector2 scale     { get; set; }
        protected Rectangle bounds  { get; set; }
        protected float rotation    { get; set; }
        protected bool isCollidable { get; set; }
        protected bool flipX        { get; set; }

        public Entity(Texture2D texture, Vector2 position){

            this.texture = texture;
            this.bounds = new Rectangle((int)position.X, (int)position.Y, (int)(texture.Width * scale.X), (int)(texture.Height * scale.Y));
            this.velocity = Vector2.Zero;
            this.origin = Vector2.Zero;
            this.scale = new Vector2(1, 1);
            this.rotation = 0;
            this.isCollidable = true;
            this.flipX = false;
        }

        virtual public void update()
        {

            bounds = new Rectangle((int)position.X, (int)position.Y, (int)texture.Width, (int)texture.Height);
        }

        virtual public void draw(SpriteBatch batch)
        {
            if (flipX)
            {
                batch.Draw(texture, position, null, Color.White, rotation, origin, scale, SpriteEffects.FlipHorizontally, 0);
            }else
            {
                batch.Draw(texture, position, null, Color.White, rotation, origin, scale, SpriteEffects.None, 0);
            }
        }

    }
}
