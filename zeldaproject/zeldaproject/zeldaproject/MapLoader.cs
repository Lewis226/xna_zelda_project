﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace ZeldaProject
{
    public class MapLoader
    {
        List<Texture2D> textureList = new List<Texture2D>();
        List<Map> mapList = new List<Map>();
        string mapFolder = @"C:\Users\Lewissss\Dropbox\XnaProjects\XnaProjects\XnaProjects\Maps";

        public MapLoader(MapAssets assets)
        {
            textureList = assets.getTexturesList();

            Console.WriteLine("Loading maps...");
            foreach (string file in Directory.EnumerateFiles(mapFolder, "*.txt"))
            {
                readMaps(file);
            }
            Console.WriteLine("Maps loaded!");
        }

        public void readMaps(string fileLocation)
        {
            List<String> lineList = new List<String>();
            string line = "";
            List<Tile> tempTiles = new List<Tile>();

            StreamReader reader = new StreamReader(fileLocation);

            string name = fileLocation.Remove(0, 31);
            name = name.Remove(name.Length - 4);

            while((line = reader.ReadLine()) != null){

                lineList.Add(line);
            }

            //Outer loop (The Y axis as its going down
            for (int y = 0; y < lineList.Count; y++)
            {
                string temp = lineList[y];
                bool collide = false;

                if (temp.Contains('#'))
                {
                    continue;
                }

                string[] values = lineList[y].Split('-');

                for (int x = 0; x < values.Length; x++)
                {
                    int id = 0;

                    if (values[x] != "")
                    {
                        if (values[x].Contains('y'))
                        {
                            collide = true;
                            values[x] = values[x].Remove(values[x].Length - 1);
                        }
                        else if (values[x].Contains('n'))
                        {
                            collide = false;
                            values[x] = values[x].Remove(values[x].Length - 1);
                        }

                        id = Convert.ToInt32(values[x]);
                        tempTiles.Add(new Tile(textureList[id], new Vector2(x * 16, y * 16), collide));
                    }
                }
            }

            mapList.Add(new Map(name, tempTiles));

            reader.Close();
        }

        public Map getMap(string mapID)
        {

            foreach (Map map in mapList)
            {
                if (map.getID() == mapID)
                {
                    Console.WriteLine("Map found!");
                    return map;
                }
            }

            return null;

        }
    }
}
