﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ZeldaProject
{
    public class MapEditor
    {

        enum Mode
        {
            TILE,
            COLLISION,
            OBJECT,
        }

        Mode mode;

        MouseState mouseState;
        Vector2 mouse = Vector2.Zero;
        MapAssets assets;

        List<Texture2D> textureList = new List<Texture2D>();
        Texture2D[,] tileArray;
        Texture2D[,] currentTextures;
        Texture2D defaultTexture;
        Texture2D vertLine;
        Texture2D horLine;
        int lineWidth = 1;
        int size = 1024;

        Rectangle GUIRect;
        int GUIsize = 418;

        Rectangle spriteSheetRect;

        public MapEditor(GraphicsDevice device, MapAssets mAssets)
        {
            Console.WriteLine("Setting up MapEditor...");

            assets = mAssets;

            this.textureList = mAssets.getTexturesList();

            Color[] color = new Color[1024];


            for (int i = 0; i < color.Length; i++)
            {
                color[i] = Color.Black;
            }

            vertLine = new Texture2D(device, lineWidth, size);
            vertLine.SetData(color);

            horLine = new Texture2D(device, size, lineWidth);
            horLine.SetData(color);

            //Set up default textures
            currentTextures = new Texture2D[64, 64];
            Color[] defaultColor = new Color[16 * 16];

            for (int i = 0; i < defaultColor.Length; i++)
            {
                defaultColor[i] = Color.LawnGreen;
            }

            defaultTexture = new Texture2D(device, 16, 16);
            defaultTexture.SetData(defaultColor);

            for (int x = 0; x < 64; x++)
            {
                for (int y = 0; y < 64; y++)
                {
                    currentTextures[x, y] = defaultTexture;
                }
            }

            GUIRect = new Rectangle(device.Viewport.Width - GUIsize, 0, GUIsize, device.Viewport.Height);

            spriteSheetRect = new Rectangle(device.Viewport.Width - GUIsize + 3, device.Viewport.Height - 427 - 4, 418, 435);

            Console.WriteLine("Map Editor ready!");
        }

        public Vector2 getMouseGridClick()
        {
            return new Vector2(mouseState.X / 16, mouseState.Y / 16);
        }

        public void setNewTexture(Vector2 mouseClick)
        {
            currentTextures[(int)mouseClick.X, (int)mouseClick.Y] = textureList[0];
        }

        public void setDefaultTexture(Vector2 mouseClick)
        {
            currentTextures[(int)mouseClick.X, (int)mouseClick.Y] = defaultTexture;
        }

        public void update()
        {


            if (mouseState.LeftButton == ButtonState.Pressed)
            {  
                setNewTexture(getMouseGridClick());
            }

            if (mouseState.RightButton == ButtonState.Pressed)
            {
                setDefaultTexture(getMouseGridClick());
            }

            mouseState = Mouse.GetState();
        }

        public void draw(SpriteBatch batch)
        {
            //Draw all textures
            for (int x = 0; x < 64; x++)
            {
                for (int y = 0; y < 64; y++)
                {
                    batch.Draw(currentTextures[x, y], new Vector2(x * 16, y * 16), Color.White);
                }
            }

            //Horizontal lines
            for (int i = 0; i < 64; i += 1)
            {
                batch.Draw(horLine, new Vector2(0, i * 16), Color.Black);
            }

            //Vertical lines
            for (int i = 0; i < 64; i += 1)
            {
                batch.Draw(vertLine, new Vector2(i * 16, 0), Color.Black);
            }

            //Draw GUI
            batch.Draw(defaultTexture, GUIRect, Color.Blue);
            //Draw SpriteSheet
            batch.Draw(defaultTexture, spriteSheetRect, Color.Red);

            //Hor Lines
            for (int i = 0; i < 27; i++)
            {
                batch.Draw(horLine, new Vector2(spriteSheetRect.X, spriteSheetRect.Y + (i * 16)), Color.Black);
            }

            //Vert lines
            for (int i = 0; i < 26; i++)
            {
                batch.Draw(vertLine, new Vector2(spriteSheetRect.X + (i * 16), spriteSheetRect.Y), Color.Black);
            }

        }


        //TODO: Draw the spritesheet, just draw a semi-transparent tile over the current tile selected!


    }
}
