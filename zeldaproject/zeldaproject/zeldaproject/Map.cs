﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace ZeldaProject
{
    public class Map
    {
        private List<Tile> tileList = new List<Tile>();
        string mapID;

        public Map(string mapID, List<Tile> tempList)
        {
            this.tileList = tempList;
            this.mapID = mapID;
        }

        public string getID()
        {
            return mapID;
        }

        public Tile getTile(int x, int y)
        {
            return tileList[x * tileList.Count + y];
        }

        public void setTile(int x, int y, Tile tile)
        {
            tileList[x * tileList.Count + y] = tile;
        }

        public void destroyMap()
        {
            for (int i = 0; i < tileList.Count; i++)
            {
                tileList.RemoveAt(i);
            }
        }

        public void drawMap(SpriteBatch batch)
        {
            foreach (Tile tile in tileList)
            {
                tile.draw(batch);
            }
        }

    }
}
